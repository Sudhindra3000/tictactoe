# Recording
https://github.com/Sudhindra3000/TicTacToe/assets/55095527/58a50a52-1d4c-4adc-b2b6-9cc642a2ae3a

# Screenshots
||||
|:----------------------------------------:|:-----------------------------------------:|:-----------------------------------------: |
| ![1 Home](https://github.com/Sudhindra3000/TicTacToe/assets/55095527/22f43bb5-d9c7-47b0-b90a-640ad057a6b1) | ![2 Player Names](https://github.com/Sudhindra3000/TicTacToe/assets/55095527/0115b7cf-5fce-4ee6-b3b7-be27ca08eab9) | ![3 Game Initial](https://github.com/Sudhindra3000/TicTacToe/assets/55095527/d65c7a26-d2f6-45ee-a53f-46003f6a88a0) |
| ![4 Playing](https://github.com/Sudhindra3000/TicTacToe/assets/55095527/9ae856d5-c6ae-45bb-bb94-9698a4dc81cd) | ![5 Winner](https://github.com/Sudhindra3000/TicTacToe/assets/55095527/7a2a3358-f4f1-4dba-bb37-6fc49b516409) |  |
