package com.sudhindra.tictactoe.utils

fun String.firstLetter(): String = first().toString()
